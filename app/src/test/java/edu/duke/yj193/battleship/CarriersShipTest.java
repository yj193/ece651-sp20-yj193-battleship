package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class CarriersShipTest {

  Coordinate c1 = new Coordinate(0, 0);
  Coordinate c2 = new Coordinate(0, 1);
  Coordinate c3 = new Coordinate(1, 0);
  Coordinate c4 = new Coordinate(1, 1);
  Coordinate c5 = new Coordinate(1, 2);
  Coordinate c6 = new Coordinate(1, 3);
  Coordinate c7 = new Coordinate(1, 4);
  Coordinate c8 = new Coordinate(0, 2);
  Coordinate c9 = new Coordinate(0, 3);
  Coordinate c10 = new Coordinate(0, 4);
  Coordinate c11 = new Coordinate(2, 0);
  Coordinate c12 = new Coordinate(3, 0);
  Coordinate c13 = new Coordinate(4, 1);
  Coordinate c14 = new Coordinate(3, 1);
  Coordinate c15 = new Coordinate(2, 1);

  @Test

  public void test_hashset_u() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c1);
    hashset.add(c3);
    hashset.add(c11);
    hashset.add(c12);

    hashset.add(c13);
    hashset.add(c14);
    hashset.add(c15);
    assertEquals(hashset, CarriersShip.makeCoords(c1, 'U'));
  }

  @Test
  public void test_hashset_l() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c5);
    hashset.add(c3);
    hashset.add(c4);
    hashset.add(c6);
    hashset.add(c8);
    hashset.add(c9);
    hashset.add(c10);
    assertEquals(hashset, CarriersShip.makeCoords(c1, 'L'));
  }

  @Test
  public void test_hashset_r() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c8);
    hashset.add(c9);
    hashset.add(c10);
    hashset.add(c2);
    hashset.add(c3);
    hashset.add(c5);
    hashset.add(c4);
    assertEquals(hashset, CarriersShip.makeCoords(c1, 'R'));
  }

  @Test
  public void test_hashset_d() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c1);
    hashset.add(c3);
    hashset.add(c11);
    hashset.add(c4);
    hashset.add(c13);
    hashset.add(c14);
    hashset.add(c15);
    assertEquals(hashset, CarriersShip.makeCoords(c1, 'D'));
  }

  @Test
  public void test_rectangleShip() {

    CarriersShip<Character> r1 = new CarriersShip<>(c1, 'U', 'c', '*');
    assertThrows(IllegalArgumentException.class, () -> new CarriersShip<Character>(c1, 'C', 'c', '*'));
    assertEquals("Carrier", r1.getName());
    // assertFalse(r1.myPieces.get(c2));
    // assertFalse(r1.myPieces.get(c3));
    // assertFalse(r1.myPieces.get(c4));

  }

  @Test
  public void test_c() {
    CarriersShip<Character> r1 = new CarriersShip<>(c1, 'U', 'c', '*');
    assertEquals(c13, r1.getUpperLeft());
    CarriersShip<Character> r2 = new CarriersShip<>(c1, 'D', 'c', '*');
    assertEquals(c1, r2.getUpperLeft());
    CarriersShip<Character> r3 = new CarriersShip<>(c1, 'L', 'c', '*');
    assertEquals(c10, r3.getUpperLeft());
    CarriersShip<Character> r4 = new CarriersShip<>(c1, 'R', 'c', '*');
    assertEquals(c3, r4.getUpperLeft());
  }

}
