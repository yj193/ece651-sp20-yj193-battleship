package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

public class ChoosePlayerTest {

  private ChoosePlayer createChoosePlayer(String inputData, OutputStream bytes, String name) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    ArrayList<String> shipsToPlace = new ArrayList<>();
    HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns = new HashMap<>();

    return new ChoosePlayer(name, board, input, output, shipFactory, shipsToPlace, shipCreationFns);
  }

  @Test
  public void test_readAction_yes() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    ChoosePlayer chooser = createChoosePlayer("YES\n", bytes, "A");
    String prompt = "Player A, are you a human player?(Y/N)\n";
    assertEquals("Y", chooser.readAction("A"));
    assertEquals(bytes.toString(),prompt);
  }

  @Test
  public void test_readAction_no() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    ChoosePlayer chooser = createChoosePlayer("No\n", bytes, "A");
    String prompt = "Player A, are you a human player?(Y/N)\n";
    assertEquals("N", chooser.readAction("A"));
    assertEquals(bytes.toString(),prompt);
  }

  @Test
  public void test_readAction_invalid() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    ChoosePlayer chooser = createChoosePlayer("ssss\n", bytes, "A");
    String prompt = "Player A, are you a human player?(Y/N)\n";
    assertEquals("", chooser.readAction("A"));
    assertEquals(prompt+
        "That action is invalid: Please enter Y/N.\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

}
