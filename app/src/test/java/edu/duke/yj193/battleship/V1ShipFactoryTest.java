package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName,
      char expectedLetter, Coordinate... expectedLocs) {
    assertEquals(testShip.getName(), expectedName);
    for (Coordinate i : expectedLocs) {
      assertEquals(testShip.getDisplayInfoAt(i,true), expectedLetter);
 assertEquals(testShip.getDisplayInfoAt(i,false), null);
    }

  }

  @Test
  public void test_Destroyer() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    Placement v1_3 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst2 = f.makeDestroyer(v1_3);
    checkShip(dst2, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
        Placement v1_4 = new Placement(new Coordinate(1, 2), 'a');
        assertThrows(IllegalArgumentException.class, () -> f.makeDestroyer(v1_4));

  }

  @Test
  public void test_Submarine() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeSubmarine(v1_2);
    checkShip(dst, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));
    Placement v1_3 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst2 = f.makeSubmarine(v1_3);
    checkShip(dst2, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

  }

  @Test
  public void test_Carrier() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeCarrier(v1_2);
    checkShip(dst, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
        new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));
    Placement v1_3 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst2 = f.makeCarrier(v1_3);
    checkShip(dst2, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
        new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));

  }

  @Test
  public void test_BattleShip() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeBattleship(v1_2);
    checkShip(dst, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
        new Coordinate(4, 2));
    Placement v1_3 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst2 = f.makeBattleship(v1_3);
    checkShip(dst2, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
        new Coordinate(1, 5));

  }
}
