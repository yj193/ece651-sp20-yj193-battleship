package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

public class AutoPlayerTest {

  @Test
  public void test_getters() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    BoardTextView view = new BoardTextView(board);
        ArrayList<String> shipsToPlace = new ArrayList<>();
    HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns = new HashMap<>();

    Player p1 = new AutoPlayer("A", board, output, shipFactory,shipsToPlace,shipCreationFns);
    assertEquals("A", p1.getName());
    assertEquals(view.displayMyOwnBoard(), p1.getBoardTextView().displayMyOwnBoard());
    assertEquals(board, p1.getBattleShipBoard());
  }
@Test
public void test_win() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    BoardTextView view = new BoardTextView(board);
        ArrayList<String> shipsToPlace = new ArrayList<>();
    HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns = new HashMap<>();
        PrintStream output2 = new PrintStream(bytes, true);
    Board<Character> board2 = new BattleShipBoard<Character>(10, 20, 'X');  
    Player p1 = new AutoPlayer("A", board, output, shipFactory,shipsToPlace,shipCreationFns);
    Player p2 = new AutoPlayer("B", board2, output2, shipFactory, shipsToPlace, shipCreationFns);

    assertTrue( p1.playOneTurn(p2));

}
}
