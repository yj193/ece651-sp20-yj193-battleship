package edu.duke.yj193.battleship;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BattleshipsShipTest {
  Coordinate c1 = new Coordinate(0, 0);
  Coordinate c2 = new Coordinate(0, 1);
  Coordinate c3 = new Coordinate(1, 0);
  Coordinate c4 = new Coordinate(1, 1);
  Coordinate c5 = new Coordinate(1, 2);
  Coordinate c6 = new Coordinate(2, 1);
  Coordinate c7 = new Coordinate(2, 0);
  Coordinate c8 = new Coordinate(0, 2);

  @Test
  public void test_hashset_u() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c2);
    hashset.add(c3);
    hashset.add(c4);
    hashset.add(c5);
    assertEquals(hashset, BattleshipsShip.makeCoords(c1, 'U'));
  }

  @Test
  public void test_hashset_l() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c2);
    hashset.add(c3);
    hashset.add(c4);
    hashset.add(c6);
    assertEquals(hashset, BattleshipsShip.makeCoords(c1, 'L'));
  }

  @Test
  public void test_hashset_r() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c1);
    hashset.add(c3);
    hashset.add(c7);
    hashset.add(c4);
    assertEquals(hashset, BattleshipsShip.makeCoords(c1, 'R'));
  }

  @Test
  public void test_hashset_d() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c1);
    hashset.add(c2);
    hashset.add(c8);
    hashset.add(c4);
    assertEquals(hashset, BattleshipsShip.makeCoords(c1, 'D'));
  }

  @Test
  public void test_battleShip() {
    BattleshipsShip<Character> r1 = new BattleshipsShip<>(c1, 'U', 's', '*');
    assertFalse(r1.myPieces.get(c2));
    assertFalse(r1.myPieces.get(c3));
    assertFalse(r1.myPieces.get(c4));
    assertEquals("Battleship", r1.getName());
    assertThrows(IllegalArgumentException.class, () -> new BattleshipsShip<Character>(c1, 'a', 's', '*'));
  }

  @Test
  public void test_c() {
    BattleshipsShip<Character> r1 = new BattleshipsShip<>(c1, 'U', 'b', '*');
    assertEquals(c5, r1.getUpperLeft());
    BattleshipsShip<Character> r2 = new BattleshipsShip<>(c1, 'D', 'b', '*');
    assertEquals(c1, r2.getUpperLeft());
    BattleshipsShip<Character> r3 = new BattleshipsShip<>(c1, 'L', 'b', '*');
    assertEquals(c2, r3.getUpperLeft());
    BattleshipsShip<Character> r4 = new BattleshipsShip<>(c1, 'R', 'b', '*');
    assertEquals(c7, r4.getUpperLeft());
  }

}
