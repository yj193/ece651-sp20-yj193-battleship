package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  Coordinate c1 = new Coordinate(1, 2);
  Coordinate c2 = new Coordinate(2, 2);
  Coordinate c3 = new Coordinate(3, 2);

  @Test
  public void test_hashset() {
    HashSet<Coordinate> hashset = new HashSet<>();
    hashset.add(c1);
    hashset.add(c2);
    hashset.add(c3);
    assertEquals(hashset, RectangleShip.makeCoords(c1, 1, 3));
  }

  @Test
  public void test_rectangleShip() {
    RectangleShip<Character> r1 = new RectangleShip<>("Submarine", c1, 1, 3, 's', '*');
    assertFalse(r1.myPieces.get(c1));
    assertFalse(r1.myPieces.get(c2));
    assertFalse(r1.myPieces.get(c3));
    assertEquals("Submarine", r1.getName());

  }

}
