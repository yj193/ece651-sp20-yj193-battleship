package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void compareContentHelper(String expectedHeader, String expectedBody, Board<Character> b1) {
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void BoardHelper(int w, int h, String expectedHeader, String expectedBody, int shipNum) {
    Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');

    if (shipNum == 1) {
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(0, 3), 's', '*')), null);
      compareContentHelper(expectedHeader, expectedBody, b1);
    }

    if (shipNum == 2) {
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(0, 3), 's', '*')), null);
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(1, 2), 's', '*')), null);
      compareContentHelper(expectedHeader, expectedBody, b1);
    }

    if (shipNum == 3) {
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(0, 3), 's', '*')), null);
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(1, 2), 's', '*')), null);
      assertEquals(b1.tryAddShip(new RectangleShip<>(new Coordinate(2, 2), 's', '*')), null);
      compareContentHelper(expectedHeader, expectedBody, b1);
    }

  }

  @Test
  public void test_display_empty_2by2() {
    emptyBoardHelper(2, 2, "  0|1\n", "A  |  A\n" + "B  |  B\n");
  }

  @Test
  public void test_display_empty_3by2() {
    emptyBoardHelper(3, 2, "  0|1|2\n", "A  | |  A\n" + "B  | |  B\n");
    String expectedHeader = "  0|1\n";
    String expectedBody = "A  |  A\n" +
        "B  |  B\n" +
        "C  |  C\n";
    emptyBoardHelper(2, 3, expectedHeader, expectedBody);

  }

  @Test
  public void test_display_empty_3by5() {
    emptyBoardHelper(3, 5, "  0|1|2\n", "A  | |  A\n" + "B  | |  B\n" + "C  | |  C\n" + "D  | |  D\n" + "E  | |  E\n");
    String expectedHeader = "  0|1|2|3|4\n";
    String expectedBody = "A  | | | |  A\n" +
        "B  | | | |  B\n" +
        "C  | | | |  C\n";
    emptyBoardHelper(5, 3, expectedHeader, expectedBody);
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<>(11, 20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<>(10, 27, 'X');
    // you should write two assertThrows here
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));

  }

  @Test
  public void test_display_nonempty_4by3() {
    BoardHelper(4, 3, "  0|1|2|3\n", "A  | | |s A\n" + "B  | | |  B\n" + "C  | | |  C\n", 1);
    BoardHelper(4, 3, "  0|1|2|3\n", "A  | | |s A\n" + "B  | |s|  B\n" + "C  | |s|  C\n", 3);
    BoardHelper(4, 3, "  0|1|2|3\n", "A  | | |s A\n" + "B  | |s|  B\n" + "C  | | |  C\n", 2);
    BoardHelper(4, 3, "  0|1|2|3\n", "A  | | |s A\n" + "B  | | |  B\n" + "C  | |s|  C\n", 0);

  }

  @Test
  public void test_displayEnemyBoard() {

    Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
    BoardTextView view = new BoardTextView(b1);
    Coordinate c1 = new Coordinate("A3");
    Coordinate c2 = new Coordinate("B0");
    V1ShipFactory sf = new V1ShipFactory();
    b1.tryAddShip(sf.makeSubmarine(new Placement(c2, 'h')));
    b1.tryAddShip(sf.makeDestroyer(new Placement(c1, 'v')));
    String myView = "  0|1|2|3\n" +
        "A  | | |d A\n" +
        "B s|s| |d B\n" +
        "C  | | |d C\n" +
        "  0|1|2|3\n";
    // make sure we laid things out the way we think we did.
    assertEquals(myView, view.displayMyOwnBoard());

    String enemView0 = "  0|1|2|3\n" +
        "A  | | |  A\n" +
        "B  | | |  B\n" +
        "C  | | |  C\n" +
        "  0|1|2|3\n";
    assertEquals(enemView0, view.displayEnemyBoard());
    b1.fireAt(c1);
    b1.fireAt(new Coordinate("C0"));
    String enemView1 = "  0|1|2|3\n" +
        "A  | | |d A\n" +
        "B  | | |  B\n" +
        "C X| | |  C\n" +
        "  0|1|2|3\n";
    assertEquals(enemView1, view.displayEnemyBoard());
    String enemView2 = "  0|1|2|3\n" +
        "A  | | |d A\n" +
        "B s|s| |d B\n" +
        "C X| | |d C\n" +
        "  0|1|2|3\n";
    b1.fireAt(new Coordinate("B1"));
    b1.fireAt(c2);
    b1.fireAt(new Coordinate("B3"));
    b1.fireAt(new Coordinate("C3"));
    assertEquals(enemView2, view.displayEnemyBoard());
  }

  @Test
  public void test_displayMyBoardWithEnemyNextToIt() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
    BoardTextView view1 = new BoardTextView(b1);
    Board<Character> b2 = new BattleShipBoard<Character>(4, 3, 'X');
    BoardTextView view2 = new BoardTextView(b1);
    
    Coordinate c1 = new Coordinate("A3");
    Coordinate c2 = new Coordinate("B0");
    V1ShipFactory sf = new V1ShipFactory();
    b1.tryAddShip(sf.makeSubmarine(new Placement(c2, 'h')));
    b1.tryAddShip(sf.makeDestroyer(new Placement(c1, 'v')));
    b1.fireAt(new Coordinate("A0"));
    
    String myView = "     Your ocean               Player B's ocean\n"+
      "  0|1|2|3                    0|1|2|3  \n" +
        "A  | | |d A                A X| | |  A\n" +
        "B s|s| |d B                B  | | |  B\n" +
        "C  | | |d C                C  | | |  C\n" +
        "  0|1|2|3                    0|1|2|3\n";
    // make sure we laid things out the way we think we did.
    assertEquals(myView, view1.displayMyBoardWithEnemyNextToIt(view2, "Your ocean", "Player B's ocean"));

  }
}
