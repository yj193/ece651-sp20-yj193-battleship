package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_collision() {
    PlacementRuleChecker<Character> checker = new NoCollisionRuleChecker<Character>(null);
    Board<Character> b1 = new BattleShipBoard<Character>(10, 10, checker, 'X');
    V1ShipFactory v1 = new V1ShipFactory();
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),null);
    b1.tryAddShip(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"));
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),"That placement is invalid: the ship overlaps another ship.\n\n\n-----------------------------------------------------------------------");

  }

  @Test
  public void test_collisionAndBound() {
    PlacementRuleChecker<Character> checker = new NoCollisionRuleChecker<Character>(
        new InBoundsRuleChecker<Character>(null));
    Board<Character> b1 = new BattleShipBoard<Character>(10, 10, checker, 'X');
    V1ShipFactory v1 = new V1ShipFactory();
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),null);
    b1.tryAddShip(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"));
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),"That placement is invalid: the ship overlaps another ship.\n\n\n-----------------------------------------------------------------------");

  }

}
