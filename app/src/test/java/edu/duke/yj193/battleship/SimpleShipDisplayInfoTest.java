package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_getInfo() {
    SimpleShipDisplayInfo<Character> s1 = new SimpleShipDisplayInfo<Character>('a', 'b');
    assertEquals('a', s1.getInfo(new Coordinate(1, 1), false));
    assertEquals('b', s1.getInfo(new Coordinate(1, 1), true));
  }

}
