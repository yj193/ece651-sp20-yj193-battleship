package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[0].length; j++) {
        assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
      }
    }
  }

  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  @Test
  public void test_no_ships() {
    Character[][] t1 = new Character[20][10];
    BattleShipBoard<Character> b1 = new BattleShipBoard<>(20, 10, 'X');
    checkWhatIsAtBoard(b1, t1);
  }

  @Test
  public void test_vaild_cases() {
    Character[][] t1 = new Character[20][10];
    BattleShipBoard<Character> b1 = new BattleShipBoard<>(20, 10, 'X');
    Character[][] t2 = new Character[15][1];
    BattleShipBoard<Character> b2 = new BattleShipBoard<>(15, 1, 'X');
    Character[][] t3 = new Character[10][10];
    BattleShipBoard<Character> b3 = new BattleShipBoard<>(19, 10, 'X');
    Character[][] t4 = new Character[1][1];
    BattleShipBoard<Character> b4 = new BattleShipBoard<>(1, 1, 'X');
    BattleShipBoard<Character> b5 = new BattleShipBoard<>(10, 20, 'X');
    t1[2][2] = 's';
    t1[6][2] = 's';
    t1[15][9] = 's';
    t2[0][0] = 's';
    t2[14][0] = 's';
    t3[2][8] = 's';
    t4[0][0] = 's';

    V1ShipFactory sf = new V1ShipFactory();
    assertEquals(
        "That placement is invalid: the ship goes off the right of the board.\n\n\n-----------------------------------------------------------------------",
        b5.tryAddShip(sf.makeSubmarine(new Placement(new Coordinate(0, 19), 'h'))));
    assertEquals(null,
        b5.tryAddShip(sf.makeSubmarine(new Placement(new Coordinate(0, 1), 'h'))));
    assertEquals(
        "That placement is invalid: the ship goes off the left of the board.\n\n\n-----------------------------------------------------------------------",
        b5.tryAddShip(sf.makeSubmarine(new Placement(new Coordinate(0, -1), 'h'))));
    assertEquals(
        "That placement is invalid: the ship goes off the top of the board.\n\n\n-----------------------------------------------------------------------",
        b5.tryAddShip(sf.makeSubmarine(new Placement(new Coordinate(-1, 0), 'h'))));
    // assertEquals("That placement is invalid: the ship goes off the top of the
    // board.\n",b5.tryAddShip(sf.makeSubmarine(new Placement(new Coordinate("scd"),
    // 'h'))));

    assertEquals(b2.tryAddShip(new RectangleShip<>(new Coordinate(0, 0), 's',
        '*')), null);
    assertEquals(b2.tryAddShip(new RectangleShip<>(new Coordinate(14, 0), 's',
        '*')),
        "That placement is invalid: the ship goes off the bottom of the board.\n\n\n-----------------------------------------------------------------------");
    assertEquals(b3.tryAddShip(new RectangleShip<>(new Coordinate(2, 8), 's',
        '*')), null);
    assertEquals(b4.tryAddShip(new RectangleShip<>(new Coordinate(0, 0), 's',
        '*')), null);

    checkWhatIsAtBoard(b3, t3);

  }

  @Test
  public void test_fireAt() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    b1.tryAddShip(dst);
    assertFalse(dst.isSunk());
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(1, 3);

    Coordinate c3 = new Coordinate(2, 2);
    Coordinate c4 = new Coordinate(3, 2);
    Coordinate c5 = new Coordinate(5, 2);
    assertSame(b1.fireAt(c1), dst);
    assertSame(b1.fireAt(c2), null);
    b1.fireAt(c3);
    assertFalse(dst.isSunk());
    b1.fireAt(c4);
    assertTrue(dst.isSunk());
    assertEquals('X', b1.whatIsAtForEnemy(c2));
    assertEquals('d', b1.whatIsAtForEnemy(c1));
  }

  @Test
  public void test_checkLose() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    b1.tryAddShip(dst);
    assertFalse(b1.checkLose());
    b1.fireAt(new Coordinate(1, 2));
    b1.fireAt(new Coordinate(2, 2));
    b1.fireAt(new Coordinate(3, 2));
    assertTrue(b1.checkLose());
  }

  @Test
  public void test_whatIsShip() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 4, 'X');
    V2ShipFactory f = new V2ShipFactory();
    Placement p1 = new Placement("A0H");
    Ship<Character> s1 = f.makeSubmarine(p1);
    b1.tryAddShip(s1);
    assertEquals(null, b1.whatIsShip(new Coordinate("B0")));
    assertSame(s1, b1.whatIsShip(new Coordinate("A0")));
    b1.removeShip(s1);
  }

  @Test
  public void test_checkCoordinate() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 4, 'X');
    Coordinate c1 = new Coordinate("A4");
    assertFalse(b1.checkCoordinate(c1));
    Coordinate c2 = new Coordinate("A3");
    assertTrue(b1.checkCoordinate(c2));

  }
}
