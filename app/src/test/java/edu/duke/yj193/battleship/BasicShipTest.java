package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class BasicShipTest {
  Coordinate c1 = new Coordinate("A1");
  Coordinate c2 = new Coordinate("B2");
  RectangleShip<Character> b1 = new RectangleShip<Character>("Submarine", c1, 1, 3, 's', '*');

  @Test
  public void test_occupiesCoordinates() {
    // assertThrows(IllegalArgumentException.class, () ->
    // b1.occupiesCoordinates(c2));
    assertFalse(b1.occupiesCoordinates(c2));
    assertTrue(b1.occupiesCoordinates(c1));
  }

  @Test
  public void test_isSunk() {
    assertEquals(b1.getName(), "Submarine");
    b1.recordHitAt(c1);
    assertFalse(b1.isSunk());

    assertTrue(b1.wasHitAt(c1));
    assertTrue(b1.occupiesCoordinates(c1));

    b1.recordHitAt(new Coordinate("B1"));
    assertThrows(IllegalArgumentException.class, () -> b1.wasHitAt(c2));
    // assertTrue(!b1.wasHitAt(c2));
    b1.recordHitAt(new Coordinate("C1"));
    assertTrue(b1.isSunk());
  }

  @Test
  public void test_getCoordinates() {
    Set<Coordinate> set = new HashSet<>();
    set.add(c1);
    set.add(new Coordinate("B1"));
    set.add(new Coordinate("C1"));
    assertEquals(set, b1.getCoordinates());
  }

  
}
