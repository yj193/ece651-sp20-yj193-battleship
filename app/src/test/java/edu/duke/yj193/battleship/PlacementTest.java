
package edu.duke.yj193.battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_where_and_orientation() {
    Coordinate c1 = new Coordinate(5, 6);
    Placement p1 = new Placement(c1, 'H');
    assertEquals(c1, p1.getWhere());
    assertEquals('H', p1.getOrientation());

    Coordinate c2 = new Coordinate(0, 6);
    Placement p2 = new Placement(c2, 'v');
    assertEquals(c2, p2.getWhere());
    assertEquals('V', p2.getOrientation());

    //    assertThrows(IllegalArgumentException.class, () -> new Placement(c2, 'a'));
    // assertThrows(IllegalArgumentException.class, () -> new Placement(c2, '0'));
  }

  @Test
  public void test_upper_case() {
    Coordinate c1 = new Coordinate(5, 6);
    Placement p1 = new Placement(c1, 'v');
    Placement p2 = new Placement(c1, 'V');
    assertEquals(p1, p2);

  }

  @Test
  public void test_equals() {
    Placement p1 = new Placement("A1H");
    Placement p2 = new Placement("c1H");
    Placement p3 = new Placement("A5v");
    Placement p4 = new Placement("A1H");
    assertEquals(p1, p1); // equals should be reflexsive
    assertEquals(p1, p4); // different objects bu same contents
    assertNotEquals(p1, p3); // different contents
    assertNotEquals(p2, p4);
    assertNotEquals(p3, p4);
    assertNotEquals(p1, "(1, 2)"); // different types
  }

  @Test
  public void test_hashCode() {
    Placement p1 = new Placement("A1H");
    Placement p2 = new Placement("c1H");
    Placement p3 = new Placement("A5v");
    Placement p4 = new Placement("A1H");
    assertEquals(p1.hashCode(), p4.hashCode());
    assertNotEquals(p1.hashCode(), p3.hashCode());
    assertNotEquals(p1.hashCode(), p2.hashCode());
  }

  @Test
  public void test_string_constructor_valid_cases() {
    Placement p1 = new Placement("A1H");
    assertEquals(0, p1.getWhere().getRow());
    assertEquals(1, p1.getWhere().getColumn());
    assertEquals('H', p1.getOrientation());
  }
@Test
public void test_toString(){
Placement p1 = new Placement("a1h");
assertEquals("A1H", p1.toString());
}
  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("A12H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A1HH"));
    //    assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A1"));
    // assertThrows(IllegalArgumentException.class, () -> new Placement("aaa"));

  }
}
