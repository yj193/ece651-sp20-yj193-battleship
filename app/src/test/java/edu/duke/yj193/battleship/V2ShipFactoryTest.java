package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName,
      char expectedLetter, Coordinate... expectedLocs) {
    assertEquals(testShip.getName(), expectedName);
    for (Coordinate i : expectedLocs) {
      assertEquals(testShip.getDisplayInfoAt(i,true), expectedLetter);
 assertEquals(testShip.getDisplayInfoAt(i,false), null);
    }

  }


  @Test
  public void test_Carrier() {
    V2ShipFactory f = new V2ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(0, 0), 'U');
    Ship<Character> dst = f.makeCarrier(v1_2);
    checkShip(dst, "Carrier", 'c', new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(2, 0),
              new Coordinate(3, 0), new Coordinate(2, 1), new Coordinate(3, 1),new Coordinate(4,1));
    Placement v1_3 = new Placement(new Coordinate(0, 0), 'L');
    Ship<Character> dst2 = f.makeCarrier(v1_3);
    checkShip(dst2, "Carrier", 'c', new Coordinate(0, 2), new Coordinate(0, 3), new Coordinate(0, 4),
              new Coordinate(1, 0), new Coordinate(1, 1), new Coordinate(1, 2),new Coordinate(1,3));

  }

  @Test
  public void test_BattleShip() {
    V2ShipFactory f = new V2ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(0, 0), 'D');
    Ship<Character> dst = f.makeBattleship(v1_2);
    checkShip(dst, "Battleship", 'b', new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(0, 2),
        new Coordinate(1, 1));
    Placement v1_3 = new Placement(new Coordinate(0, 0), 'R');
    Ship<Character> dst2 = f.makeBattleship(v1_3);
    checkShip(dst2, "Battleship", 'b', new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(2, 0),
        new Coordinate(1, 1));

  }
}
