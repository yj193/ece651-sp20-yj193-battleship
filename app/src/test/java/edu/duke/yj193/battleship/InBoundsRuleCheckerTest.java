package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_placement() {
    PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
    Board<Character> b1 = new BattleShipBoard<Character>(10, 10, checker, 'X');
    V1ShipFactory v1 = new V1ShipFactory();
    assertEquals(
                 checker.checkMyRule(v1.createShip(new Placement(new Coordinate("Z2"), 'v'), 1, 2, 's', "Submarine"), b1), "That placement is invalid: the ship goes off the bottom of the board.\n\n\n-----------------------------------------------------------------------");
    assertEquals(
               checker.checkMyRule(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),null);
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("Z2"), 'v'), 1, 2, 's', "Submarine"), b1),"That placement is invalid: the ship goes off the bottom of the board.\n\n\n-----------------------------------------------------------------------");
    assertEquals(
                 checker.checkPlacement(v1.createShip(new Placement(new Coordinate("A2"), 'v'), 1, 2, 's', "Submarine"), b1),null);

  }

}
