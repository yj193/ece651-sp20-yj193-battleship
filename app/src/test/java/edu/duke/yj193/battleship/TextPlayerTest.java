package edu.duke.yj193.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

public class TextPlayerTest {

  private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    ArrayList<String> shipsToPlace = new ArrayList<>();
    HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns = new HashMap<>();

    return new TextPlayer("A", board, input, output, shipFactory, shipsToPlace, shipCreationFns);
  }

  private TextPlayer createTextPlayerV2(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V2ShipFactory shipFactory = new V2ShipFactory();
    ArrayList<String> shipsToPlace = new ArrayList<>();
    HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns = new HashMap<>();

    return new TextPlayer("A", board, input, output, shipFactory, shipsToPlace, shipCreationFns);
  }

  @Test
  void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');
    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]); // did we get the right Placement back
      assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt and newline
      bytes.reset(); // clear out bytes for next time around
    }
  }

  @Test
  void test_read_placement_wrongcase() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "A0Q\n", bytes);
    String prompt = "Please enter a location for a ship:";
    Placement p = player.readPlacement(prompt);
    // assertEquals(prompt + "\n" + "That placement is invalid: it does not have the
    // correct format.\n", bytes.toString());
    bytes.reset();
  }

  @Test
  void test_doOnePlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "J3V\n", bytes);
    StringBuilder expected = new StringBuilder();
    String prompt = "Player A where do you want to place a Destroyer?\n";
    String header = "  0|1|2|3|4|5|6|7|8|9\n";
    String body = "A  | | | | | | | | |  A\n" +
        "B  | | | | | | | | |  B\n" +
        "C  | | | | | | | | |  C\n" +
        "D  | | | | | | | | |  D\n" +
        "E  | | | | | | | | |  E\n" +
        "F  | | | | | | | | |  F\n" +
        "G  | | | | | | | | |  G\n" +
        "H  | | | | | | | | |  H\n" +
        "I  | | | | | | | | |  I\n" +
        "J  | | |d| | | | | |  J\n" +
        "K  | | |d| | | | | |  K\n" +
        "L  | | |d| | | | | |  L\n" +
        "M  | | | | | | | | |  M\n" +
        "N  | | | | | | | | |  N\n" +
        "O  | | | | | | | | |  O\n" +
        "P  | | | | | | | | |  P\n" +
        "Q  | | | | | | | | |  Q\n" +
        "R  | | | | | | | | |  R\n" +
        "S  | | | | | | | | |  S\n" +
        "T  | | | | | | | | |  T\n";
    expected.append(prompt);
    expected.append(header);
    expected.append(body);
    expected.append(header + "\n");
    player.doOnePlacement("Destroyer", (p) -> new V1ShipFactory().makeDestroyer(p));
    assertEquals(expected.toString(), bytes.toString());
    bytes.reset();
  }

  // @Test
  // void test_doPlacementPhase() throws IOException {

  // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  // TextPlayer player = createTextPlayer(10, 20, "J3V\n", bytes);
  // StringBuilder expected = new StringBuilder();
  // String prompt = "Player A where do you want to place a Destroyer?\n";
  // String header = " 0|1|2|3|4|5|6|7|8|9\n";
  // String body = "A | | | | | | | | | A\n" +
  // "B | | | | | | | | | B\n" +
  // "C | | | | | | | | | C\n" +
  // "D | | | | | | | | | D\n" +
  // "E | | | | | | | | | E\n" +
  // "F | | | | | | | | | F\n" +
  // "G | | | | | | | | | G\n" +
  // "H | | | | | | | | | H\n" +
  // "I | | | | | | | | | I\n" +
  // "J | | | | | | | | | J\n" +
  // "K | | | | | | | | | K\n" +
  // "L | | | | | | | | | L\n" +
  // "M | | | | | | | | | M\n" +
  // "N | | | | | | | | | N\n" +
  // "O | | | | | | | | | O\n" +
  // "P | | | | | | | | | P\n" +
  // "Q | | | | | | | | | Q\n" +
  // "R | | | | | | | | | R\n" +
  // "S | | | | | | | | | S\n" +
  // "T | | | | | | | | | T\n";
  // String body2 = "A | | | | | | | | | A\n" +
  // "B | | | | | | | | | B\n" +
  // "C | | | | | | | | | C\n" +
  // "D | | | | | | | | | D\n" +
  // "E | | | | | | | | | E\n" +
  // "F | | | | | | | | | F\n" +
  // "G | | | | | | | | | G\n" +
  // "H | | | | | | | | | H\n" +
  // "I | | | | | | | | | I\n" +
  // "J | | |d| | | | | | J\n" +
  // "K | | |d| | | | | | K\n" +
  // "L | | |d| | | | | | L\n" +
  // "M | | | | | | | | | M\n" +
  // "N | | | | | | | | | N\n" +
  // "O | | | | | | | | | O\n" +
  // "P | | | | | | | | | P\n" +
  // "Q | | | | | | | | | Q\n" +
  // "R | | | | | | | | | R\n" +
  // "S | | | | | | | | | S\n" +
  // "T | | | | | | | | | T\n";

  // String content = "Player A: you are going to place the following ships (which
  // are all rectangular). For each ship, type the coordinate of the upper left
  // side of the ship, followed by either H (for horizontal) or V (for vertical).
  // For example M4H would place a ship horizontally starting at M4 and going to
  // the right. You have\n\n2 \"Submarines\" ships that are 1x2\n3 \"Destroyers\"
  // that are 1x3\n3 \"Battleships\" that are 1x4\n2 \"Carriers\" that are 1x6\n";
  // // expected.append(prompt);

  // expected.append(header);
  // expected.append(body);
  // expected.append(header + "\n");
  // expected.append(content);
  // expected.append(prompt);
  // expected.append(header);
  // expected.append(body2);
  // expected.append(header + "\n");

  // player.doPlacementPhase();
  // assertEquals(expected.toString(), bytes.toString());

  // bytes.reset();
  // }

  @Test
  public void test_playOneTurn() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "a\nf\nsss\na1\n", bytes);
    StringBuilder expected = new StringBuilder();
    String prompt = "Player A's turn:\n";
    String myView = "     Your ocean               Player A's ocean\n" +
        "  0|1|2|3                    0|1|2|3  \n" +
        "A  | | |  A                A  | | |  A\n" +
        "B  | | |  B                B  | | |  B\n" +
        "C  | | |  C                C  | | |  C\n" +
        "  0|1|2|3                    0|1|2|3\n";
    String myView2 = "     Your ocean               Player A's ocean\n" +
        "  0|1|2|3                    0|1|2|3  \n" +
        "A  | | |  A                A  |X| |  A\n" +
        "B  | | |  B                B  | | |  B\n" +
        "C  | | |  C                C  | | |  C\n" +
        "  0|1|2|3                    0|1|2|3\n";
    String mid = "\n"
        + "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (3 remaining)\nS Sonar scan (3 remaining)\nThat action is invalid: Please enter F/M/S.\n\n\n-----------------------------------------------------------------------\n"

        + "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (3 remaining)\nS Sonar scan (3 remaining)\n"

        + "Please attack!\n";

    expected.append(prompt);
    expected.append(myView);
    expected.append(mid);
    expected.append(
        "Invalid coordinate, please enter a valid one!\n\n\n-----------------------------------------------------------------------\n");
    expected.append("You missed!\n");
    expected.append("Player A win, player A lose!\n");
    expected.append(myView2 + "\n");

    player.playOneTurn(player);
    assertEquals(expected.toString(), bytes.toString());
    bytes.reset();

    // @Test
    // public void test_helperRead() throws IOException {
    // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    // TextPlayer player2 = createTextPlayer(4, 3, "sss\n", bytes);
    // player2.helper
    // AssertThrowIllegalException("");

    // }
    // }
  }

  @Test
  public void test_readAction_f1() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "F\n", bytes);
    String s = player.readAction("A", 2, 3);
    assertEquals("F", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nS Sonar scan (3 remaining)\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_f2() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "F\n", bytes);
    String s = player.readAction("A", 0, 0);
    assertEquals("F", s);
    assertEquals("", bytes.toString());
  }

  @Test
  public void test_readAction_length_not_one() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "ab\n", bytes);
    String s = player.readAction("A", 2, 3);
    assertEquals("", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nS Sonar scan (3 remaining)\nThat action is invalid: Please input length of 1.\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_m_is_zero() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "M\n", bytes);
    String s = player.readAction("A", 0, 3);
    assertEquals("", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nS Sonar scan (3 remaining)\nThat action is invalid: Please enter F/S.\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_s_is_zero() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "S\n", bytes);
    String s = player.readAction("A", 2, 0);
    assertEquals("", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nThat action is invalid: Please enter F/M.\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_invaild_input() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "c\n", bytes);
    String s = player.readAction("A", 2, 1);
    assertEquals("", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nS Sonar scan (1 remaining)\nThat action is invalid: Please enter F/M/S.\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_vaild() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "s\n", bytes);
    String s = player.readAction("A", 2, 3);
    assertEquals("S", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nS Sonar scan (3 remaining)\n",
        bytes.toString());
  }

  @Test
  public void test_readAction_null() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 3, "", bytes);
    String s = player.readAction("A", 2, 3);
    assertEquals("", s);
    assertEquals(
        "Possible actions for Player A:\n\nF Fire at a square\nM Move a ship to another square (2 remaining)\nS Sonar scan (3 remaining)\nThe input is null! Please enter again!\n\n\n-----------------------------------------------------------------------\n",
        bytes.toString());
  }

  // @Test
  // public void test_helperSonar() throws IOException {
  // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  // Board<Character> board = new BattleShipBoard<Character>(5, 5, 'X');
  // V2ShipFactory shipFactory = new V2ShipFactory();
  // board.tryAddShip(shipFactory.makeBattleship(new Placement("A0U")));

  // }

  @Test
  public void test_doOneSonarScan_c() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer me = createTextPlayerV2(10, 20, "S\nD3\n", bytes);
    Board<Character> enemyBoard = new BattleShipBoard<Character>(10, 20, 'X');
    V2ShipFactory sf = new V2ShipFactory();
    enemyBoard.tryAddShip(sf.makeCarrier(new Placement("b4u")));
    Coordinate c = new Coordinate("D3");
    String result = me.heplerReadHashMap(me.heplerSonar(c, enemyBoard));
    StringBuilder expected = new StringBuilder();
    String res = "Submarines occupy 0 squares\n" +
        "Destroyers occupy 0 squares\n" +
        "Battleships occupy 0 squares\n" +
        "Carriers occupy 6 squares\n";
    expected.append(res);
    //    assertEquals(expected.toString(), result);

  }
}
