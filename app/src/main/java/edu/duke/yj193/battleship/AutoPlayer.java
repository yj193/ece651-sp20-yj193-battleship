package edu.duke.yj193.battleship;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class AutoPlayer implements Player {
  private String name;
  final Board<Character> theBoard;
  private final BoardTextView view;
  private final PrintStream out;
  private final AbstractShipFactory<Character> shipFactory;
  private final ArrayList<String> shipsToPlace;
  private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  private final Random rand;

  /**
   * Constructs textplayer with the Board, inputSource, out print stream
   * and BoardTextView
   * 
   * @param theBoard    is the board of the new app
   * @param inputSource is the input of the user
   * @param out         is the output result
   */
  public AutoPlayer(String name, Board<Character> theBoard, PrintStream out,
      AbstractShipFactory<Character> shipFactory, ArrayList<String> shipsToPlace,
      HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns) {
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.out = out;
    this.shipFactory = shipFactory;
    this.name = name;
    this.shipsToPlace = shipsToPlace;
    this.shipCreationFns = shipCreationFns;
    setupShipCreationMap();
    setupShipCreationList();
    this.rand = new Random(999);
  }

  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
  }

  protected void setupShipCreationList() {
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));

  }

  @Override
  public Placement readPlacement(String prompt) throws IOException {
    if (prompt == "Submarine" || prompt == "Destroyer") {
        // add sed
        Placement p = new Placement(new Coordinate(rand.nextInt(20), rand.nextInt(10)), 'H');
        // Placement p = new Placement(new Coordinate((int) (Math.random() * 20), (int)
        // (Math.random() * 10)), 'H');
        return p;
    } else {
        // add seed
        Placement p = new Placement(new Coordinate(rand.nextInt(20), rand.nextInt(10)), 'U');
        // Placement p = new Placement(new Coordinate((int) (Math.random() * 20), (int)
        // (Math.random() * 10)), 'U');
        return p;
    }
  }

  @Override
  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    preDoOnePlacement(shipName, createFn);
  }

  @Override
  public Ship<Character> preDoOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn)
      throws IOException {
    Placement p = readPlacement(shipName);
    Ship<Character> s = findShip(shipName, createFn, p);
    String error = theBoard.tryAddShip(s);
    if (error != null) {
      return preDoOnePlacement(shipName, createFn);
    }
    return s;

  }

  /**
   *
   * findShip find the vaild ship according to the ship function, catch the
   * exception and recursively run doOnePlacement
   *
   * @throws IOException if input or output errors
   * @param shipName the name of the ship
   * @param createFn the create ship funtion
   * 
   */

  private Ship<Character> findShip(String shipName, Function<Placement, Ship<Character>> createFn, Placement p){
      Ship<Character> s = createFn.apply(p);
      return s;

  }

  @Override
  public void doPlacementPhase() throws IOException {
    for (String shipName : shipsToPlace) {
      doOnePlacement(shipName, shipCreationFns.get(shipName));
    }
  }

  @Override
  public boolean playOneTurn(Player enemy) throws IOException {
    Coordinate c1 = new Coordinate(rand.nextInt(20), rand.nextInt(10));
    // add seed
    // Coordinate c1 = new Coordinate((int) (Math.random() * 20), (int)
    // (Math.random() * 10));
    Ship<Character> s = enemy.getBattleShipBoard().fireAt(c1);
    if (s == null) {
      out.println("Player " + this.name + " missed!");
    } else {
      out.println("Player " + this.name + " hit a " + s.getName() + "!");
    }
    if (enemy.getBattleShipBoard().checkLose()) {
      out.println("Player " + this.name + " win, player " + enemy.getName() + " lose!");
      return true;
    } else {
      return false;
    }
  }

  @Override
  public Board<Character> getBattleShipBoard() {
    return theBoard;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public BoardTextView getBoardTextView() {
    return view;
  }
}
