package edu.duke.yj193.battleship;

import java.io.IOException;
import java.util.HashMap;
import java.util.function.Function;

public interface Player {
  public Placement readPlacement(String prompt) throws IOException;

  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException;

  public Ship<Character> preDoOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn)
      throws IOException;

  public void doPlacementPhase() throws IOException;

  public boolean playOneTurn(Player enemy) throws IOException;

  public Board<Character> getBattleShipBoard();

  public String getName();

  public BoardTextView getBoardTextView();
}
