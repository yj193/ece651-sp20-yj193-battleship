package edu.duke.yj193.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer implements Player {
  private String name;
  final Board<Character> theBoard;
  private final BoardTextView view;
  private final BufferedReader inputReader;
  private final PrintStream out;
  private final AbstractShipFactory<Character> shipFactory;
  private final ArrayList<String> shipsToPlace;
  private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  private int moveNum;
  private int scanNum;

  /**
   * Constructs textplayer with the Board, inputSource, out print stream
   * and BoardTextView
   * 
   * @param theBoard    is the board of the new app
   * @param inputSource is the input of the user
   * @param out         is the output result
   */
  public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out,
      AbstractShipFactory<Character> shipFactory, ArrayList<String> shipsToPlace,
      HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns) {
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = inputSource;
    this.out = out;
    this.shipFactory = shipFactory;
    this.name = name;
    this.shipsToPlace = shipsToPlace;
    this.shipCreationFns = shipCreationFns;
    setupShipCreationMap();
    setupShipCreationList();
    this.moveNum = 3;
    this.scanNum = 3;
  }

  /**
   *
   * readPlacement change the input string into a Placement object
   * 
   * @param prompt is the default input
   * @return new Placement
   * @throws IOException if input or output errors
   */
  public Placement readPlacement(String prompt) throws IOException {
    out.println(prompt);
    String s = inputReader.readLine();
    try {
      Placement p = new Placement(s);
      return p;
    } catch (IllegalArgumentException e) {
      out.println("That placement is invalid: it does not have the correct format.");
      out.println("\n\n-----------------------------------------------------------------------"); // return
                                                                                                  // readPlacement(prompt);
      return null;
    }
  }

  /**
   *
   * readAction reads the action of the user, decide what users want to do
   * 
   * @param name    is the user name
   * @param moveNum is the left number of moving a ship to another square
   * @param scanNum is the left number of Sonar scan
   * @return specific action
   * @throws IOException if input or output errors
   */
  protected String readAction(String name, int moveNum, int scanNum) throws IOException {
    if (moveNum <= 0 && scanNum <= 0) {
      return "F";
    }

    out.println("Possible actions for Player " + name + ":\n");
    out.println("F Fire at a square");
    if (moveNum > 0) {
      out.println("M Move a ship to another square (" + moveNum + " remaining)");
    }
    if (scanNum > 0) {
      out.println("S Sonar scan (" + scanNum + " remaining)");
    }
    try {
      String s = inputReader.readLine();
      if (s.length() != 1) {
        out.println("That action is invalid: Please input length of 1.");
        out.println("\n\n-----------------------------------------------------------------------");
        return "";
      }

      s = s.toUpperCase();
      if (s.equals("F") || s.equals("M") || s.equals("S")) {
        if (moveNum <= 0 && s.equals("M")) {
          out.println("That action is invalid: Please enter F/S.");
          out.println("\n\n-----------------------------------------------------------------------");
          return "";
        } else if (scanNum <= 0 && s.equals("S")) {
          out.println("That action is invalid: Please enter F/M.");
          out.println("\n\n-----------------------------------------------------------------------");
          return "";
        } else {
          return s;
        }
      } else {
        out.println("That action is invalid: Please enter F/M/S.");
        out.println("\n\n-----------------------------------------------------------------------");
        return "";
      }
    }

    catch (Exception e) {
      out.println("The input is null! Please enter again!");
      out.println("\n\n-----------------------------------------------------------------------");
      return "";
    }

  }

  /**
   * Read the action
   * 
   * @throws IOException if input or output errors
   * @return action such as F, M, S
   */
  private String helperReadAction() throws IOException {
    String s = readAction(name, moveNum, scanNum);
    while (s == "") {
      s = readAction(name, moveNum, scanNum);
    }
    return s;
  }

  /**
   *
   * doOnePlacement adds the ship into board
   * 
   * @throws IOException if input or output errors
   * @param shipName the name of the ship
   * @param createFn the create ship funtion
   */

  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    preDoOnePlacement(shipName, createFn);
    out.println(view.displayMyOwnBoard());
  }

  public Ship<Character> preDoOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn)
      throws IOException {
    Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
    if (p == null) {
      return preDoOnePlacement(shipName, createFn);
    }
    Ship<Character> s = findShip(shipName, createFn, p);
    if (s == null) {
      return preDoOnePlacement(shipName, createFn);
    }
    String error = theBoard.tryAddShip(s);
    if (error != null) {
      out.println(error);
      return preDoOnePlacement(shipName, createFn);
    }
    return s;
  }

  /**
   *
   * findShip find the vaild ship according to the ship function, catch the
   * exception and recursively run doOnePlacement
   *
   * @throws IOException if input or output errors
   * @param shipName the name of the ship
   * @param createFn the create ship funtion
   * 
   */

  private Ship<Character> findShip(String shipName, Function<Placement, Ship<Character>> createFn, Placement p)
      throws IOException {
    try {
      Ship<Character> s = createFn.apply(p);
      return s;
    } catch (IllegalArgumentException e) {
      out.println(e.getMessage());
    }
    return null;
  }

  public void doPlacementPhase() throws IOException {
    out.println(view.displayMyOwnBoard());
    String content = "Player " + name
        + ": you are going to place the following ships (which are all rectangular). For each ship, type the coordinate of the upper left side of the ship, followed by either H (for horizontal) or V (for vertical).  For example M4H would place a ship horizontally starting at M4 and going to the right.  You have\n\n2 \"Submarines\" ships that are 1x2\n3 \"Destroyers\" that are 1x3\n3 \"Battleships\" that are 1x4\n2 \"Carriers\" that are 1x6";
    out.println(content);
    for (String shipName : shipsToPlace) {
      doOnePlacement(shipName, shipCreationFns.get(shipName));
    }
  }

  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
  }

  protected void setupShipCreationList() {
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));

  }

  /**
   * display the current MyBoardWithEnemyNextToIt, and make a fire turn
   *
   * @param enemy is another player
   * @param btw   is the enemy's board text view
   * @throws IOException if input or output errors
   */

  public boolean playOneTurn(Player enemy) throws IOException {
    out.println("Player " + this.name + "'s turn:");
    String line = view.displayMyBoardWithEnemyNextToIt(enemy.getBoardTextView(), "Your ocean",
        "Player " + enemy.getName() + "'s ocean");
    out.println(line);
    String action = helperReadAction();
    switch (action) {
      case "F":
        out.println("Please attack!");
        Coordinate c1 = helperRead();
        Ship<Character> s = enemy.getBattleShipBoard().fireAt(c1);
        if (s == null) {
          out.println("You missed!");
        } else {
          out.println("You hit a " + s.getName() + "!");
        }
        break;
      case "M":
        moveNum--;
        out.println("Please choose a ship you want to move!");
        Ship<Character> prevShip = helperReadMove();
        Ship<Character> newShip = preDoOnePlacement(prevShip.getName(), shipCreationFns.get(prevShip.getName()));
        newShip.recordHitAfterMove(prevShip);
        theBoard.removeShip(prevShip);
        break;
      case "S":
        scanNum--;
        out.println("Please choose a coordiante you want to add sonar!");
        Coordinate c3 = helperRead();
        String res = this.heplerReadHashMap(this.heplerSonar(c3, enemy.getBattleShipBoard()));
        out.println(res);
        break;
    }

    if (enemy.getBattleShipBoard().checkLose()) {
      out.println("Player " + this.name + " win, player " + enemy.getName() + " lose!");
      String res = view.displayMyBoardWithEnemyNextToIt(enemy.getBoardTextView(), "Your ocean",
          "Player " + enemy.getName() + "'s ocean");
      out.println(res);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Read the coordinate for attacking on the board
   * 
   * @throws IOException if input or output errors
   * @return Coordinate to fire at
   */
  protected Coordinate helperRead() throws IOException {
    while (true) {
      try {
        String s = inputReader.readLine();
        Coordinate c = new Coordinate(s);
        return c;
      } catch (IllegalArgumentException e) {
        out.println("Invalid coordinate, please enter a valid one!");
        out.println("\n\n-----------------------------------------------------------------------");
      }
    }

  }

  /**
   * Read the coordinate for move the ship
   * 
   * @throws IOException if input or output errors
   * @return ship to move
   */
  protected Ship<Character> helperReadMove() throws IOException {
    while (true) {
      Coordinate c = helperRead();
      Ship<Character> ship = theBoard.whatIsShip(c);
      if (ship != null) {
        return ship;
      }
      out.println("In this coordinate, there is no valid ship, please enter a valid one!");
      out.println("\n\n-----------------------------------------------------------------------");
    }
  }

  /**
   * Read the coordinate for adding a sonar
   *
   * @param the coordinate to add the sonar
   * @return a hashmap, key is the type of ship, value is the number of basic
   *         ships
   */
  protected HashMap<String, Integer> heplerSonar(Coordinate c, Board<Character> enemy) {
    HashMap<String, Integer> hashmap = new HashMap<>();
    hashmap.put("Submarine", 0);
    hashmap.put("Destroyer", 0);
    hashmap.put("Battleship", 0);
    hashmap.put("Carrier", 0);
    for (int i = -3; i < 4; i++) {
      for (int j = -3; j < 4; j++) {
        if (Math.abs(i) + Math.abs(j) <= 3) {
          Coordinate check = new Coordinate(c.getRow() + i, c.getColumn() + j);
          if (theBoard.checkCoordinate(check)) {
            Ship<Character> ship = enemy.whatIsShip(check);
            if (ship != null) {
              hashmap.put(ship.getName(), hashmap.get(ship.getName()) + 1);
            }
          }
        }
      }
    }
    return hashmap;
  }

  /**
   * Read the hashmap, display the sonar result
   *
   * @param the hashMap searched by the sonar
   * @return a string of the search result
   * 
   */
  protected String heplerReadHashMap(HashMap<String, Integer> hashmap) {
    StringBuffer sb = new StringBuffer();
    for (String s : hashmap.keySet()) {
      sb.append(s + "s occupy " + hashmap.get(s) + "  squares\n");
    }
    return sb.toString();
  }

  @Override
  public Board<Character> getBattleShipBoard() {
    return theBoard;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public BoardTextView getBoardTextView() {
    return view;
  }

}
