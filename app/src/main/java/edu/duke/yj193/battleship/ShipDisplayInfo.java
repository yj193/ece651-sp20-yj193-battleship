package edu.duke.yj193.battleship;

public interface ShipDisplayInfo<T> {
       public T getInfo(Coordinate where, boolean hit);

}

