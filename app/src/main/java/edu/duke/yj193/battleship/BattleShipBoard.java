package edu.duke.yj193.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  private final ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementChecker;
  private final HashSet<Coordinate> enemyMisses;
  private final T missInfo;

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  /**
   * Constructs a BattleShipBoard with the specified width
   * and height
   * 
   * @param w is the width of the newly constructed board.
   * @param h is the height of the newly constructed board.
   * @throws IllegalArgumentException if the width or height are less than or
   *                                  equal to zero.
   */
  public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementChecker, T missInfo) {
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
    }
    if (h <= 0) {

      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
    }

    this.width = w;
    this.height = h;
    this.myShips = new ArrayList<>();
    this.placementChecker = placementChecker;
    this.enemyMisses = new HashSet<Coordinate>();
    this.missInfo = missInfo;
  }

  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);
  }

  /**
   *
   * tryAddShip adds a Ship<T> toAdd into myShips
   * 
   * @param toAdd is the new ship needs to be added into myships
   * @return if the toAdd is added into myShips, return true
   *
   */
  public String tryAddShip(Ship<T> toAdd) {
    String res = this.placementChecker.checkPlacement(toAdd, this);
    if (res == null) {
      myShips.add(toAdd);
    }
    return res;

  }

  /**
   *
   * whatIsAt checks if this coordinate has a ship or not
   * 
   * @param where is the location needs to be searched
   * @return null or the content in this location
   *
   */
  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  public T whatIsAtForEnemy(Coordinate where) {
    T res = whatIsAt(where, false);
    if (enemyMisses.contains(where)) {
      return missInfo;
    }
    return res;
  }

  protected T whatIsAt(Coordinate where, boolean isSelf) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s.getDisplayInfoAt(where, isSelf);
      }
    }
    return null;
  }

  /**
   *
   * whatIsShip return the related ship accoring to the input coordinate
   * 
   * @param where is the location needs to be searched
   * @return null or the ship
   *
   */

  public Ship<T> whatIsShip(Coordinate where) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s;
      }
    }
    return null;
  }

  /**
   * This method should search for any ship that occupies coordinate c
   * 
   * @param c is the coordinate to fire at
   *          x * @return the ship hit at c, null if it is a miss.
   */
  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(c) && !s.wasHitAt(c)) {
        s.recordHitAt(c);
        return s;
      }
    }
    enemyMisses.add(c);
    return null;
  }

  /**
   * This method makes a decision if the game finishes
   * 
   * @return false if the user doesn't lose, true if the user loses the game
   */

  public boolean checkLose() {
    for (Ship<T> ship : myShips) {
      if (!ship.isSunk())
        return false;
    }
    return true;
  }

  /**
   * This method makes a old ship and remove it from the board
   *
   * @param oldShip is the ship need to be deleted
   */
  public void removeShip(Ship<T> oldShip) {
    myShips.remove(oldShip);
  }

  /**
   * This method is used to check if the coordinate is inside the battleShipBoard,
   * return the result of boolean
   * 
   * @param the coordinate needs to be checked
   * @return the boolean result, true if it is in the board, false if not
   */
  public boolean checkCoordinate(Coordinate c) {
    if (c.getColumn() < this.getWidth() && c.getColumn() >= 0 && c.getRow() < this.getHeight() && c.getRow() >= 0) {
      return true;
    }
    return false;
  }
}
