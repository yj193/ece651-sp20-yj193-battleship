package edu.duke.yj193.battleship;

public interface Board<T> {
  public int getWidth();

  public int getHeight();

  public String tryAddShip(Ship<T> toAdd);

  public T whatIsAtForSelf(Coordinate where);

  public T whatIsAtForEnemy(Coordinate where);

  public Ship<T> fireAt(Coordinate c);

  public boolean checkLose();

  public Ship<T> whatIsShip(Coordinate where);

  public void removeShip(Ship<T> oldShip);

  public boolean checkCoordinate(Coordinate c);
}
