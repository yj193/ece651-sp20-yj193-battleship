package edu.duke.yj193.battleship;

import java.io.EOFException;

public class Coordinate {
  private final int row;
  private final int column;

  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }

  /**
   * Constructs a Coordinate with the specified row
   * and column
   * 
   * @param r is the row of the newly Coordinate.
   * @param c is the column of the newly Coordinate.
   * @throws IllegalArgumentException if the row or column are less than zero.
   */
  public Coordinate(int r, int c) {
    this.row = r;
    this.column = c;
  }

  /**
   * Constructs a Coordinate with the input of a string coordinate and output a
   * specified row and column
   * 
   * @param r is the row of the newly Coordinate.
   * @throws IllegalArgumentException if the row is not frome A to Z or column is
   *                                  less than zero or larger than 9.
   */
  public Coordinate(String descr) {
    if (descr.length() != 2) {
      throw new IllegalArgumentException("Coordinate's input must have both row and column");
    }
    char rowLetter = descr.toUpperCase().charAt(0);
    char columnLetter = descr.charAt(1);
    if (rowLetter < 'A' || rowLetter > 'Z') {
      throw new IllegalArgumentException("Coordinate's row must from A to T but is " + rowLetter);
    }
    if (columnLetter < '0' || columnLetter > '9') {
      throw new IllegalArgumentException("Coordinate's row must from A to T but is " + rowLetter);
    }

    this.row = rowLetter - 'A';
    this.column = Character.getNumericValue(columnLetter);

  }

  /**
   * override a equals method here
   * 
   * @param o is the pointer to object
   * @return if the input object is same as the current object, return true, else
   *         return false
   */
  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }
    return false;
  }

  /**
   * override a toString method here
   * 
   * @return the string of the location
   *
   */
  @Override
  public String toString() {
    return "(" + row + ", " + column + ")";
  }

  /**
   * override a hashCode method here
   * 
   * @return the new hashCode acccording to the current object's toString() method
   *
   */
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
