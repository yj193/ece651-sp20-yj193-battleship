package edu.duke.yj193.battleship;

public class V2ShipFactory extends V1ShipFactory {

  @Override
  public Ship<Character> makeBattleship(Placement where) {
    BattleshipsShip<Character> battleship = new BattleshipsShip<>(where.getWhere(), where.getOrientation(), 'b', '*');
    return battleship;
  }

  @Override
  public Ship<Character> makeCarrier(Placement where) {
    CarriersShip<Character> Carrier = new CarriersShip<>(where.getWhere(), where.getOrientation(), 'c', '*');
    return Carrier;
  }

}
