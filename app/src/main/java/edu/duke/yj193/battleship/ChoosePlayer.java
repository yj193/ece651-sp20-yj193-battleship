package edu.duke.yj193.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public class ChoosePlayer {
  private String name;
  final Board<Character> theBoard;
  private final BoardTextView view;
  private final BufferedReader inputReader;
  private final PrintStream out;
  private final AbstractShipFactory<Character> shipFactory;
  private final ArrayList<String> shipsToPlace;
  private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

  /**
   * Constructs a App with the Board, inputSource, out print stream
   * and BoardTextView
   * 
   * @param theBoard    is the board of the new app
   * @param inputSource is the input of the user
   * @param out         is the output result
   */
  public ChoosePlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out,
      AbstractShipFactory<Character> shipFactory, ArrayList<String> shipsToPlace,
      HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns) {
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = inputSource;
    this.out = out;
    this.shipFactory = shipFactory;
    this.name = name;
    this.shipsToPlace = shipsToPlace;
    this.shipCreationFns = shipCreationFns;
  }

  public Player choose() throws IOException {
    if(helperReadAction()){
      return new TextPlayer(name,theBoard, inputReader,out,shipFactory,shipsToPlace,shipCreationFns);
    }else{
      return new AutoPlayer(name,theBoard, out,shipFactory,shipsToPlace,shipCreationFns);
    }
  }

  /**
   *
   * readAction reads the action of the user, decide what users want to do
   * 
   * @param name is the user name
   * @return String of yes or no
   * @throws IOException if input or output errors
   */
  public String readAction(String name) throws IOException {
    out.println("Player " + name + ", are you a human player?(Y/N)");
      String s = inputReader.readLine();
      s = s.toUpperCase();
      if (s.equals("YES") || s.equals("Y")) {
        return "Y";
      } else if (s.equals("NO") || s.equals("N")) {
        return "N";
      } else {
        out.println("That action is invalid: Please enter Y/N.");
        out.println("\n\n-----------------------------------------------------------------------");
        return "";
      }

  }

  /**
   * Read the action
   * 
   * @throws IOException if input or output errors
   * @return boolean of yes or no
   */
  private boolean helperReadAction() throws IOException {
    String s = readAction(name);
    while (s == "") {
      s = readAction(name);
    }
    if (s.equals("Y")) {
      return true;
    } else {
      return false;
    }
  }

}
