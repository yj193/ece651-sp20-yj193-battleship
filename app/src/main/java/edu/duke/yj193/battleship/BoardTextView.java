package edu.duke.yj193.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;

  /**
   * Constructs a BoardView, given the board it will display.
   * 
   * @param toDisplay is the Board to display
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }

  }

  /**
   * This makes the board content, including the header line and the body lines
   * 
   * @return the String that is the display of the empty board for the given board
   */

  public String displayMyOwnBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

  protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder ans = new StringBuilder("");
    ans.append(makeHeader());
    String sep = "";
    char word = 'A';

    for (int row = 0; row < toDisplay.getHeight(); row++) {
      ans.append(word);
      ans.append(" ");
      for (int column = 0; column < toDisplay.getWidth(); column++) {
        ans.append(sep);
        if (getSquareFn.apply(new Coordinate(row, column)) == null) {
          ans.append(" ");
        } else {
          ans.append(getSquareFn.apply(new Coordinate(row, column)));
        }
        sep = "|";
      }
      sep = "";
      ans.append(" ");
      ans.append(word);
      word++;
      ans.append("\n");
    }
    ans.append(makeHeader());
    return ans.toString();
  }

  /**
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep = ""; // start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    StringBuilder sb = new StringBuilder("");
    String[] myBoard = this.displayMyOwnBoard().split("\n");
    String[] enemBoard = enemyView.displayEnemyBoard().split("\n");
    int count = 0;
    myBoard[0] = myBoard[0] + "  ";
    enemBoard[0] = enemBoard[0] + "  ";
    myBoard[myBoard.length - 1] = myBoard[0] + "  ";
    //    enemBoard[enemBoard.length - 1] = enemBoard[0] + "  ";
    while (count < 5) {
      sb.append(" ");
      count++;
    }
    sb.append(myHeader);
    count += myHeader.length();
    while (count < 2 * this.toDisplay.getWidth() + 22) {
      sb.append(" ");
      count++;
    }

    sb.append(enemyHeader);
    sb.append("\n");

    for (int i = 0; i < myBoard.length; i++) {
      count = 0;
      sb.append(myBoard[i]);
      count += myBoard[i].length();
      while (count < 2 * this.toDisplay.getWidth() + 19) {
        sb.append(" ");
        count++;
      }
      sb.append(enemBoard[i]);
      sb.append("\n");
    }
    return sb.toString();
  }
}
