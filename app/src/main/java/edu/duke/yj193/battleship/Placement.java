package edu.duke.yj193.battleship;

public class Placement {
  private final Coordinate where;
  private final char orientation;

  public Coordinate getWhere() {
    return where;
  }

  public char getOrientation() {
    return orientation;
  }

  /**
   * Constructs a placement with the specificed Coordinate and orientation
   *
   * @param where       is the Coordinate of the placement
   * @param orientation is the orientation of the placement
   * @throws IllegalArgumentException if the orientation is not H and V.
   */
  public Placement(Coordinate where, char orientation) {
    this.where = where;
    orientation = Character.toUpperCase(orientation);
    this.orientation = orientation;
  }

  /**
   * Constructs a placement with the specificed Coordinate and orientation
   *
   * @param plac is the coordinate and the orientation of the placement
   * @throws IllegalArgumentException if the length of the placement is not 3 or
   *                                  the orientation is not H and V.
   */
  public Placement(String plac) {
    if (plac.length() != 3) {
      throw new IllegalArgumentException("Coordinate's input must have row, column and orientation");
    }
    plac = plac.toUpperCase();
    this.where = new Coordinate(plac.substring(0, 2));
    // if (plac.charAt(2) != 'H' && plac.charAt(2) != 'V') {
    //   throw new IllegalArgumentException("Orientation only can be H or V");
    // }
    this.orientation = plac.charAt(2);
  }

  /**
   * override a equals method here
   * 
   * @param o is the pointer to object
   * @return if the input object is same as the current object, return true, else
   *         return false
   */
  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Placement c = (Placement) o;
      return where.getRow() == c.where.getRow() && where.getColumn() == c.where.getColumn()
          && orientation == c.getOrientation();
    }
    return false;
  }

  /**
   * override a toString method here
   * 
   * @return the string of the location and orientation
   *
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append((char)(where.getRow() + 'A'));
    sb.append(where.getColumn());
    sb.append(orientation);
    return sb.toString();
  }

  /**
   * override a hashCode method here
   * 
   * @return the new hashCode acccording to the current object's toString() method
   *
   */
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
