package edu.duke.yj193.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
  private final String name;
  private final Coordinate c;

  public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,
      ShipDisplayInfo<T> enemyDisplayInfo) {
    super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
    this.name = name;
    this.c = upperLeft;
  }

  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
        new SimpleShipDisplayInfo<T>(null, data));
  }

  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testship", upperLeft, 1, 1, data, onHit);
  }

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
    HashSet<Coordinate> hashset = new HashSet<>();
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        hashset.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
      }
    }
    return hashset;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Coordinate getUpperLeft() {
    return c;
  }
}
