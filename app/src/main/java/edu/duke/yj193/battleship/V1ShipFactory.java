package edu.duke.yj193.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
  protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
    RectangleShip<Character> r1 = new RectangleShip<>(name, where.getWhere(), w, h, letter, '*');
    RectangleShip<Character> r2 = new RectangleShip<>(name, where.getWhere(), h, w, letter, '*');
    if (where.getOrientation() == 'V') {
      return r1;
    } else if (where.getOrientation() == 'H') {
      return r2;
    } else {
      throw new IllegalArgumentException("This orientation is not V or H!\n\n\n-----------------------------------------------------------------------");
    }
  }

  @Override
  public Ship<Character> makeSubmarine(Placement where) {
    return createShip(where, 1, 2, 's', "Submarine");
  }

  @Override
  public Ship<Character> makeBattleship(Placement where) {
    return createShip(where, 1, 4, 'b', "Battleship");
  }

  @Override
  public Ship<Character> makeCarrier(Placement where) {
    return createShip(where, 1, 6, 'c', "Carrier");
  }

  @Override
  public Ship<Character> makeDestroyer(Placement where) {
    return createShip(where, 1, 3, 'd', "Destroyer");
  }

}
