package edu.duke.yj193.battleship;

import java.util.HashMap;
import java.util.HashSet;

public abstract class BasicShip<T> implements Ship<T> {
  protected HashMap<Coordinate, Boolean> myPieces;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  protected HashSet<Double> attactedPieces;

  /**
   * Constructs a BasicShip with the specified Coordinate c
   * 
   * @param where is the coordinate of this ship
   */

  public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
    myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate w : where) {
      myPieces.put(w, false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.attactedPieces = new HashSet<Double>();
  }

  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    return myPieces.containsKey(where);

  }

  @Override
  public boolean isSunk() {
    for (Coordinate c : myPieces.keySet()) {
      if (myPieces.get(c) == false)
        return false;
    }
    return true;
  }

  @Override
  public void recordHitAt(Coordinate where) {
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
    attactedPieces.add(Math.pow(this.getUpperLeft().getColumn() - where.getColumn(), 2)
        + Math.pow(this.getUpperLeft().getRow() - where.getRow(), 2));
  }

  @Override
  public boolean wasHitAt(Coordinate where) {
    checkCoordinateInThisShip(where);
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip) {
    if (myShip) {
      return myDisplayInfo.getInfo(where, wasHitAt(where));
    } else {
      return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }
  }

  protected void checkCoordinateInThisShip(Coordinate c) {
    if (!myPieces.containsKey(c))
      throw new IllegalArgumentException("The coordinate " + c.toString() + " is not set into the ship");

  }

  @Override
  public Iterable<Coordinate> getCoordinates() {
    return myPieces.keySet();
  }

  @Override
  public HashSet<Double> getDistance() {
    return attactedPieces;
  }

  @Override
  public void recordHitAfterMove(Ship<Character> prevShip) {
    Coordinate upperLeft = this.getUpperLeft();
    for (Coordinate c : getCoordinates()) {
      if (prevShip.getDistance().contains(
          Math.pow(upperLeft.getColumn() - c.getColumn(), 2) + Math.pow(upperLeft.getRow() - c.getRow(), 2))) {
        this.recordHitAt(c);
      }
    }
  }
}
