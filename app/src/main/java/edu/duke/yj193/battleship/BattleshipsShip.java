package edu.duke.yj193.battleship;

import java.util.HashSet;

public class BattleshipsShip<T> extends BasicShip<T> {
  private Coordinate c;

  public BattleshipsShip(Coordinate upperLeft, char type, ShipDisplayInfo<T> myDisplayInfo,
      ShipDisplayInfo<T> enemyDisplayInfo) {
    super(makeCoords(upperLeft, type), myDisplayInfo, enemyDisplayInfo);
    switch (type) {
      case 'U':
        this.c = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2);
        break;
      case 'D':
        this.c = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
        break;
      case 'R':
        this.c = new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn());
        break;
      case 'L':
        this.c = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1);
        break;
    }
  }

  public BattleshipsShip(Coordinate upperLeft, char type, T data, T onHit) {
    this(upperLeft, type, new SimpleShipDisplayInfo<T>(data, onHit),
        new SimpleShipDisplayInfo<T>(null, data));
  }

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char type) {
    HashSet<Coordinate> hashset = new HashSet<>();
    switch (type) {
      case 'U':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
        }
        hashset.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
        break;
      case 'D':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + i));
        }
        hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
        break;
      case 'R':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
        }
        hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
        break;
      case 'L':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + 1));
        }
        hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
        break;
      default:
        throw new IllegalArgumentException("The orientation is not U or D or L or R!\n\n\n-----------------------------------------------------------------------");
    }

    return hashset;
  }

  @Override
  public String getName() {
    return "Battleship";
  }

  @Override
  public Coordinate getUpperLeft() {
    return c;
  }

}
