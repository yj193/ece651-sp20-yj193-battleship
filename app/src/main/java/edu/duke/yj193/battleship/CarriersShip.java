package edu.duke.yj193.battleship;

import java.util.HashSet;

public class CarriersShip<T> extends BasicShip<T> {
  private Coordinate c;
  public CarriersShip(Coordinate upperLeft, char type, ShipDisplayInfo<T> myDisplayInfo,
      ShipDisplayInfo<T> enemyDisplayInfo) {
    super(makeCoords(upperLeft, type), myDisplayInfo, enemyDisplayInfo);
    switch (type) {
      case 'U':
        this.c = new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1);
        break;
      case 'D':
        this.c = new Coordinate(upperLeft.getRow(), upperLeft.getColumn());
        break;
      case 'R':
        this.c = new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn());
        break;
      case 'L':
        this.c = new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4);
        break;
    }

  }

  public CarriersShip(Coordinate upperLeft, char type, T data, T onHit) {
    this(upperLeft, type, new SimpleShipDisplayInfo<T>(data, onHit),
        new SimpleShipDisplayInfo<T>(null, data));
  }

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char type) {
    HashSet<Coordinate> hashset = new HashSet<>();
    switch (type) {
      case 'U':
        for (int i = 0; i < 4; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
        }
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + 2 + i, upperLeft.getColumn() + 1));
        }
        break;
      case 'D':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn()));
        }
        for (int i = 0; i < 4; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + 1 + i, upperLeft.getColumn() + 1));
        }
        break;
      case 'R':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
        }
        for (int i = 0; i < 4; i++) {
          hashset.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1 + i));
        }
        break;
      case 'L':
        for (int i = 0; i < 3; i++) {
          hashset.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2 + i));
        }
        for (int i = 0; i < 4; i++) {
          hashset.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + i));
        }
        break;
      default:
        throw new IllegalArgumentException("The orientation is not U or D or L or R!\n\n\n-----------------------------------------------------------------------");
    }

    return hashset;
  }
@Override
  public String getName() {
    return "Carrier";
  }

  @Override
  public Coordinate getUpperLeft() {
    return c;
  }

}
